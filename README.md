# Docker Volume Plugin for GlusterFS

This work is based on [https://github.com/vieux/docker-volume-sshfs](https://github.com/vieux/docker-volume-sshfs) which is programmed by [Victor Vieux](https://github.com/vieux) and released under MIT license! Many thanks for your clean code!

Documentation is work in progress...

# Docker Volume Plugin for glusterfs

There are quite a few plugins already which can be used to mount a glusterfs into a Docker volume. But either they are old (> 2 years) or the code isn't really structured. Therefore I decide to write a new one based on the SSHFS plugin mentioned above because we need it at work and ... just for fun. :smile:

# Prerequisites

This plugin just executes the `mount` command to mount a glusterfs filesystem and therefore expects that the GlusterFS client software (FUSE) is installed and running in your Docker host.

Furthermore an already existing glusterfs filesystem in the GlusterFS cluster ist required.

The following Docker Volume Plugin parameters are mandatory:
~~~yaml
     driver_opts:
       mountstring: server1:/VOLUMENAME
~~~


**Optional**: If your Kernel supports it, you can add any number of additional mount parameters by specifying them via the `-o` command line switch or by specifying them as yaml option in Docker Swarm yaml (see below)


# Installation

First, install the plugin:
~~~bash
> docker plugin install n0r1skcom/docker-volume-glusterfs
Plugin "n0r1skcom/docker-volume-glusterfs" is requesting the following privileges:
 - network: [host]
 - mount: [/var/lib/docker/plugins/]
 - capabilities: [CAP_SYS_ADMIN]
Do you grant the above permissions? [y/N] y
latest: Pulling from n0r1skcom/docker-volume-glusterfs
3a2b9eff8a35: Download complete 
Digest: sha256:008800aebadf9ae3fcb15e9d4bd9a695a8835e814c8d333f9d07b7ca85686046
Status: Downloaded newer image for n0r1skcom/docker-volume-glusterfs:latest
Installed plugin n0r1skcom/docker-volume-glusterfs
~~~

After this, check if the plugin is loaded and enabled:
~~~bash
> docker plugin ls
ID                  NAME                                    DESCRIPTION                ENABLED
8e9cdeec3312        n0r1skcom/docker-volume-glusterfs:latest   glusterfs plugin for Docker   true
~~~

# Enable debug mode
During installation:
~~~bash
> docker plugin install n0r1skcom/docker-volume-glusterfs DEBUG=1
~~~

After installation:
~~~bash
> docker plugin disable n0r1skcom/docker-volume-glusterfs
> docker plugin set n0r1skcom/docker-volume-glusterfs DEBUG=1
~~~

Check if debug is enabled:
~~~bash
> docker plugin inspect n0r1skcom/docker-volume-glusterfs --format "{{ .Settings.Env }}"
[DEBUG=1]
~~~

# Docker manual usage

Create a Docker volume:
~~~bash
> docker volume create -d n0r1skcom/docker-volume-glusterfs -o mountstring=server1:/GLUSTERVOLUME
~~~

**Optional**: Add more parameters to for the glusterfs mount like `mds_namespace`:
~~~bash
> docker volume create -d n0r1skcom/docker-volume-glusterfs -o mountstring=server1:/GLUSTERVOLUME -o optional=mountparameter glusterfs_data
~~~

Check if the volume is created:
~~~bash
> docker volume ls
DRIVER                                  VOLUME NAME
n0r1skcom/docker-volume-glusterfs:latest   glusterfs_data
~~~

Start a container with the volume as mount and list the content of the mounted glusterfs:
~~~
> docker run --rm -v glusterfs_data:/data alpine ls /data
lala
test
~~~

In the mounts, the glusterfs mount is now visible:
~~~bash
> mount | grep gluster
~~~

# Docker Swarm usage

Docker Swarm file to show the Docker Volumne plugin usage:
~~~yaml
version: "3"

services:

  app:
    image: nginx
    deploy:
      restart_policy:
        condition: any
      mode: replicated
      replicas: 2
    volumes:
      - data:/data

volumes:
  data:
    driver: n0r1skcom/docker-volume-glusterfs
    driver_opts:
      mountstring: server1:GLUSTERVOLUME
      #mds_namespace: yournamespace // Any kind of Kernel supported mount parameters are possible
~~~

Deploy the stack:
~~~bash
> docker stack deploy -c  test-swarm.yml test
Creating network test_default
Creating service test_app
~~~

List the volumes:
~~~
> docker volume ls
n0r1skcom/docker-volume-glusterfs:latest   test_data
~~~

Check if the data is in one of the replicas:
~~~
docker exec -ti test_app.1.p7yrkjsnfaykpw0q9fuszmplc ls /data
lala  test
~~~

# LICENSE

MIT
