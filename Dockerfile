FROM golang:1.10.4-stretch as builder
COPY . /go/src/github.com/n0r1skcom/docker-volume-glusterfs
WORKDIR /go/src/github.com/n0r1skcom/docker-volume-glusterfs
RUN go build


FROM ubuntu:16.04
COPY gluster-ppa-gpg.key /gluster-ppa-gpg.key
COPY gluster-ubuntu-glusterfs-3_12-xenial.list /gluster-ubuntu-glusterfs-3_12-xenial.list
COPY --from=builder /go/src/github.com/n0r1skcom/docker-volume-glusterfs/docker-volume-glusterfs .
RUN mkdir -p /run/docker/plugins /mnt/state /mnt/volumes && apt-key add /gluster-ppa-gpg.key && apt-get update && apt-get -y install glusterfs-client attr && apt-get clean

CMD ["docker-volume-glusterfs"]
